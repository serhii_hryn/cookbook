import config from './config';
import axios from 'axios';

export const http = () => {
  const httpConfig = {
    baseURL: config.API_URL,
  };

  const axiosInstance = axios.create(httpConfig);

  return axiosInstance;
};
