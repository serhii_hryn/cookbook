import React, { useEffect, useState } from 'react';
import { recipeService } from '../../services/recipe.service';
import RecipeList from '../RecipeList/RecipeList';
import RecipeDetailsToolbar from '../RecipeDetailsToolbar/RecipeDetailsToolbar';
import { useSelector } from 'react-redux';

const RecipeDetails = () => {
  const recipe = useSelector(state => state.recipe.currentRecipe);
  const [relatedRecipes, setRelatedRecipes] = useState([]);

  useEffect(() => {
    let isMounted = true;

    (async () => {
      const fetchedRelatedRecipes = await recipeService.getRelatedReciped(recipe._id);
      if (isMounted) {
        setRelatedRecipes(fetchedRelatedRecipes);
      }
    })();

    return () => { isMounted = false; };
  }, [recipe._id]);

  return (
    <div>
      <RecipeDetailsToolbar recipe={recipe} />
      <h1>{recipe.title}</h1>
      <div className="description">
        <p>{recipe.description}</p>
      </div>
      <div className="related-recipes-list">
        <h3>Related recipes</h3>
        <RecipeList recipes={relatedRecipes} />
      </div>
    </div>
  );
};

export default RecipeDetails;
