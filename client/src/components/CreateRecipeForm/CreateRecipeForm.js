import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { createRecipe, setNewRecipe } from '../../redux/actions/recipe.actions';
import Button from '../UI/Button/Button';
import Select from '../UI/Select/Select';
import TextArea from '../UI/TextArea/TextArea';
import Input from '../UI/Input/Input';
import { useQuery } from '../../hooks/router.hooks';
import './CreateRecipeForm.scss';

const CreateRecipeForm = () => {
  const dispatch = useDispatch();
  const { newRecipe } = useSelector(state => state.recipe);
  const parentId = useQuery().get('parentId');

  const recipeTypes = [
    { label: 'Main dish', value: 'main-dish' },
    { label: 'Beverage', value: 'beverage' },
    { label: 'Desserts', value: 'dessert' },
  ];

  const inputChangeHandler = (event) => {
    const data = {
      [event.target.name]: event.target.value,
    };
    dispatch(setNewRecipe({ ...newRecipe, ...data }));
  };

  const canSave = useMemo(() => {
    return !(newRecipe.title && newRecipe.type);
  }, [newRecipe]);

  const createRecipeHandler = () => {
    dispatch(createRecipe({ ...newRecipe, parentId: parentId || null }));
    dispatch(setNewRecipe({}));
  };

  return (
    <div className="create-recipe-form">
      <Input
        label="Title"
        name="title"
        id="title"
        type="text"
        value={newRecipe.title}
        onChange={inputChangeHandler} />
      <TextArea
        label="Description"
        name="description"
        id="desctiption"
        type="text"
        value={newRecipe.title}
        onChange={inputChangeHandler} />
      {/* <Input
        label="Related recipe"
        name="parentId"
        id="parentId"
        type="text"
        onChange={inputChangeHandler} /> */}
      <Select
        label="Type"
        name="type"
        id="type"
        options={recipeTypes}
        selectedOption={newRecipe.type}
        onChange={inputChangeHandler} />

      <Button title="Save" disabled={canSave} onClick={createRecipeHandler} />
    </div>
  );
};

CreateRecipeForm.propTypes = {
  id: PropTypes.string,
};

export default CreateRecipeForm;
