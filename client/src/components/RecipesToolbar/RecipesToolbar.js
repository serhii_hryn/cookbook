import React from 'react';
import { useHistory } from 'react-router-dom';
import Button from '../UI/Button/Button';
import './RecipesToolbar.scss';

const RecipesToolbar = () => {
  const history = useHistory();
  return (
    <div className="recipes__toolbar">
      <Button title="New Recipe" onClick={() => history.push('/recipe/create')} />
    </div>
  );
};

export default RecipesToolbar;
