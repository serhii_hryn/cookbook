import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import CreateRecipePage from '../../pages/CreateRecipePage';
import RecipeDetailsPage from '../../pages/RecipeDetailsPage';
import RecipesPage from '../../pages/RecipesPage';
import Header from '../Header/Header';
import Navbar from '../Navbar/Navbar';
import './App.scss';

function App() {
  return (
    <>
      <Header />
      <Navbar />
      <div className="container">
        <Switch>
          <Route path="/recipes" exact component={RecipesPage} />
          <Route path="/recipes/:id" exact component={RecipeDetailsPage} />
          <Route path="/recipe/create" component={CreateRecipePage} />
          {/* TODO create the EditRecipePage component */}
          <Route path="/recipe/edit/:id" component={CreateRecipePage} />
          <Redirect to="/recipes?type=main-dish" />
        </Switch>
      </div>
    </>
  );
}

export default App;
