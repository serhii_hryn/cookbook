import React from 'react';
import { useSelector } from 'react-redux';
import history from '../../history';
import Button from '../UI/Button/Button';
import './RecipeDetailsToolbar.scss';

const RecipeDetailsToolbar = () => {
  const recipe = useSelector(state => state.recipe.currentRecipe);

  return (
    <div className="recipe-details-toolbar">
      <div className="recipe-details-toolbar__section">
        <Button
          title="Back"
          onClick={() => history.goBack()} />
      </div>
      <div className="recipe-details-toolbar__section">
        <Button
          title="New Recipe"
          onClick={() => history.push(`/recipe/create?parentId=${recipe._id}`)} />
        <Button
          title="Edit"
          onClick={() => history.push(`/recipe/edit/${recipe._id}`)} />
      </div>
    </div>
  );
};

export default RecipeDetailsToolbar;
