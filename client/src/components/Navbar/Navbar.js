import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.scss';

function isQueryPathMatch(location, queryPath) {
  return location.pathname + location.search === queryPath;
}

const Navbar = () => {
  return (
    <nav className="navbar">
      <NavLink
        to="/recipes?type=main-dish"
        className="navbar__item"
        isActive={(_, location) => isQueryPathMatch(location, '/recipes?type=main-dish')}
        activeClassName="navbar__item--active"
      >
        Main Dishes
      </NavLink>
      <NavLink
        to="/recipes?type=beverage"
        className="navbar__item"
        isActive={(_, location) => isQueryPathMatch(location, '/recipes?type=beverage')}
        activeClassName="navbar__item--active"
      >
        Beverage
      </NavLink>
      <NavLink
        to="/recipes?type=dessert"
        className="navbar__item"
        isActive={(_, location) => isQueryPathMatch(location, '/recipes?type=dessert')}
        activeClassName="navbar__item--active"
      >
        Desserts
      </NavLink>

    </nav>
  );
};

export default Navbar;
