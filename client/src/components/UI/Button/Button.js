import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ title, disabled = false, onClick }) => {
  return (
    <div>
      <button onClick={onClick} disabled={disabled}>
        {title}
      </button>
    </div>
  );
};

Button.propTypes = {
  title: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

export default Button;
