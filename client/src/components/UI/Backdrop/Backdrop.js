import React from 'react';
import PropTypes from 'prop-types';
import './Backdrop.scss';

const Backdrop = ({ children, show, clickHandler }) => {
  return (
    show
      ? <div
        className="backdrop"
        onClick={clickHandler}>{children}</div>
      : null
  );
};

Backdrop.propTypes = {
  children: PropTypes.element,
  show: PropTypes.bool.isRequired,
  clickHandler: PropTypes.func,
};

export default Backdrop;
