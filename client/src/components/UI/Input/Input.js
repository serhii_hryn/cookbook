import React from 'react';
import PropTypes from 'prop-types';
import './Input.scss';

const Input = ({ label, value, name, id, type, onChange }) => {
  return (
    <div className="input">
      <label className="label" htmlFor={id}>{label}</label>
      <input
        id={id}
        name={name}
        type={type}
        value={value}
        onChange={onChange} />
    </div>
  );
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export default Input;
