import React from 'react';
import PropTypes from 'prop-types';
import Backdrop from '../Backdrop/Backdrop';
import './Modal.scss';
import Button from '../Button/Button';

const Modal = ({ children, show, onHide, onSave }) => {
  return (
    <>
      <Backdrop show={show} clickHandler={onHide} />
      <div className="modal__window"
        style={{
          transform: show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: show ? 1 : 0,
        }}>
        <div className="modal">
          <div className="modal__header">
            <span>Header</span>
            <span onClick={onHide}>&times;</span>
          </div>
          <div className="modal__content">{children}</div>
          <div className="modal__footer">
            {onSave ? <Button title="Save" onClick={onSave} /> : null}
            <Button title="Cancel" onClick={onHide} />
          </div>
        </div>
      </div>
    </>
  );
};

Modal.propTypes = {
  children: PropTypes.element,
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  onSave: PropTypes.func,
};

export default Modal;
