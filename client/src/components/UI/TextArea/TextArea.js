import React from 'react';
import PropTypes from 'prop-types';
import './TextArea.scss';

const TextArea = ({ label, value, name, id, onChange }) => {
  return (
    <div className="text-area">
      <label className="label" htmlFor={id}>{label}</label>
      <textarea
        id={id}
        name={name}
        onChange={onChange}
        rows="5"
        value={value} />
    </div>
  );
};

TextArea.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

export default TextArea;
