import React from 'react';
import PropTypes from 'prop-types';
import './Select.scss';

const Select = ({ label, name, id, options = [], selectedOption, onChange }) => {
  return (
    <div className="select">
      <label className="label" htmlFor={id}>{label}</label>
      <select
        id={id}
        name={name}
        value={selectedOption}
        onChange={onChange}>
        <option value="">-- choose value --</option>
        {options.map(option => {
          return (
            <option
              key={option.value}
              value={option.value}>
              {option.label}
            </option>
          );
        })}
      </select>
    </div>
  );
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOption: PropTypes.string,
  onChange: PropTypes.func,
};

export default Select;
