import React from 'react';
import './Header.scss';

const Header = () => {
  const toogleMenu = () => {
    const navbar = document.getElementsByClassName('navbar')[0];
    navbar.classList.toggle('navbar--open');
  };

  return (
    <header className="header">
      <div className="header__menu-icon" onClick={toogleMenu}>
        <i className="material-icons">menu</i>
      </div>
      <div className="app-title">
        <span>Cookbook</span>
      </div>
    </header>
  );
};

export default Header;
