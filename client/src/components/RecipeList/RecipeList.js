import React from 'react';
import PropTypes from 'prop-types';
import RecipeListItem from '../RecipeListItem/RecipeListItem';

const RecipeList = ({ recipes }) => {
  if (!recipes.length) {
    return <p>No recipes yet</p>;
  }

  return (
    <div className="recipe-list">
      {recipes.map(recipe => <RecipeListItem key={recipe._id} recipe={recipe} />)}
    </div>
  );
};

RecipeList.propTypes = {
  recipes: PropTypes.array.isRequired,
};

export default RecipeList;
