import React, { useEffect } from 'react';
import RecipeList from '../components/RecipeList/RecipeList';
import { useDispatch, useSelector } from 'react-redux';
import { searchRecipes } from '../redux/actions/recipe.actions';
import RecipesToolbar from '../components/RecipesToolbar/RecipesToolbar';
import { useQuery } from '../hooks/router.hooks';

const RecipesPage = () => {
  const dispatch = useDispatch();
  const { recipes, loading } = useSelector(state => state.recipe);
  const type = useQuery().get('type');

  useEffect(() => {
    dispatch(searchRecipes({ type, parentId: null }));
  }, [dispatch, type]);

  if (loading) {
    return (
      <p>Loading</p>
    );
  }

  return (
    <div className="recipes__page">
      <div className="recipes__page__content">
        <RecipesToolbar />
        <RecipeList recipes={recipes} />
      </div>
    </div>
  );
};

export default RecipesPage;
