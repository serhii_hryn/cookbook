import React from 'react';
import CreateRecipeForm from '../components/CreateRecipeForm/CreateRecipeForm';

const CreateRecipePage = () => {
  return (
    <div>
      <CreateRecipeForm />
    </div>
  );
};


export default CreateRecipePage;
