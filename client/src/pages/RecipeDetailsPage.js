import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getRecipeById } from '../redux/actions/recipe.actions';
import RecipeDetails from '../components/RecipeDetails/RecipeDetails';

const RecipeDetailsPage = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { loading } = useSelector(state => state.recipe);

  useEffect(() => {
    dispatch(getRecipeById(id));
  }, [id, dispatch]);

  if (loading) {
    return (
      <p>Loading</p>
    );
  }

  return (
    <RecipeDetails />
  );
};


export default RecipeDetailsPage;
