/* eslint-disable no-undef */
const { REACT_APP_API_URL } = process.env;

export default {
  API_URL: REACT_APP_API_URL,
};
