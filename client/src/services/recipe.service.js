import { http } from '../http';

export const recipeService = {
  async getRecipes() {
    return (await http().get('/recipe')).data;
  },
  async getRecipeById(id) {
    return (await http().get(`/recipe/${id}`)).data;
  },
  async getRelatedReciped(parentId) {
    return (await http().post('/recipe/search', { parentId })).data;
  },
  async searchRecipes(searchBody) {
    return (await http().post('/recipe/search', searchBody)).data;
  },
  async createRecipe(recipe) {
    return (await http().post('/recipe', recipe)).data;
  },
};
