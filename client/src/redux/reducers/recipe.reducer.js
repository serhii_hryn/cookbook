import {
  LOADING_START,
  LOADING_END,
  GET_RECIPES_SUCCESS,
  SET_NEW_RECIPE,
  CREATE_RECIPE_SUCCESS,
  SEARCH_RECIPES_SUCCESS,
  GET_RECIPE_BY_ID_SUCCESS
} from '../types/recipe.types';

const initialState = {
  recipes: [],
  newRecipe: {},
  currentRecipe: {},
  loading: false,
  error: null,
};

export const recipeReducer = (state = initialState, action) => {
  // TODO add cases for errors
  switch (action.type) {
    case LOADING_START:
      return { ...state, loading: true };
    case LOADING_END:
      return { ...state, loading: false };
    case GET_RECIPES_SUCCESS:
      return { ...state, recipes: action.payload.data };
    case GET_RECIPE_BY_ID_SUCCESS:
      return { ...state, currentRecipe: action.payload.data };
    case CREATE_RECIPE_SUCCESS:
      return { ...state, recipes: [...state.recipes, action.payload.data] };
    case SEARCH_RECIPES_SUCCESS:
      return { ...state, recipes: [...action.payload.data] };
    case SET_NEW_RECIPE:
      return { ...state, newRecipe: action.payload.data };
    default:
      return { ...state };
  }
};
