import {
  LOADING_START,
  LOADING_END,
  GET_RECIPES,
  GET_RECIPES_SUCCESS,
  GET_RECIPES_ERROR,
  SEARCH_RECIPES,
  SEARCH_RECIPES_SUCCESS,
  SEARCH_RECIPES_ERROR,
  CREATE_RECIPE,
  CREATE_RECIPE_SUCCESS,
  CREATE_RECIPE_ERROR,
  SET_NEW_RECIPE,
  GET_RECIPE_BY_ID,
  GET_RECIPE_BY_ID_SUCCESS,
  GET_RECIPE_BY_ID_ERROR
} from '../types/recipe.types';

export const recipeStartLoading = () => ({ type: LOADING_START });
export const recipeEndLoading = () => ({ type: LOADING_END });

export const getRecipes = () => ({ type: GET_RECIPES });
export const getRecipesSucceeded = recipes => ({ type: GET_RECIPES_SUCCESS, payload: { data: recipes } });
export const getRecipesError = error => ({ type: GET_RECIPES_ERROR, payload: { error } });

export const getRecipeById = id => ({ type: GET_RECIPE_BY_ID, payload: { data: id } });
export const getRecipeByIdSucceeded = recipe => ({ type: GET_RECIPE_BY_ID_SUCCESS, payload: { data: recipe } });
export const getRecipeByIdError = error => ({ type: GET_RECIPE_BY_ID_ERROR, payload: { error } });

export const searchRecipes = searchBody => ({ type: SEARCH_RECIPES, payload: { data: searchBody } });
export const searchRecipesSucceeded = recipes => ({ type: SEARCH_RECIPES_SUCCESS, payload: { data: recipes } });
export const searchRecipesError = error => ({ type: SEARCH_RECIPES_ERROR, payload: { error } });

export const createRecipe = recipe => ({ type: CREATE_RECIPE, payload: { data: recipe } });
export const createRecipeSuccess = recipe => ({ type: CREATE_RECIPE_SUCCESS, payload: { data: recipe } });
export const createRecipeError = error => ({ type: CREATE_RECIPE_ERROR, paload: { error } });

export const setNewRecipe = newRecipe => ({ type: SET_NEW_RECIPE, payload: { data: newRecipe } });
