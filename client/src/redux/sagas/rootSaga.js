import { all } from 'redux-saga/effects';
import { recipeSagas } from './recipe.saga';

export function* rootSaga() {
  yield all([...recipeSagas]);
}
