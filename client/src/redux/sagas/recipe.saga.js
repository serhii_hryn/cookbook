import { put, call, takeEvery } from 'redux-saga/effects';
import history from '../../history';
import { recipeStartLoading, getRecipesSucceeded, getRecipesError, recipeEndLoading, createRecipeError, createRecipeSuccess, searchRecipesError, searchRecipesSucceeded, getRecipeByIdSucceeded, getRecipeByIdError } from '../actions/recipe.actions';
import { recipeService } from '../../services/recipe.service';
import { GET_RECIPES, CREATE_RECIPE, SEARCH_RECIPES, GET_RECIPE_BY_ID } from '../types/recipe.types';

function* getRecipesWorker() {
  try {
    yield put(recipeStartLoading());
    const recipes = yield call(recipeService.getRecipes);
    yield put(getRecipesSucceeded(recipes));
  } catch (error) {
    yield put(getRecipesError(error));
  } finally {
    yield put(recipeEndLoading());
  }
}

function* getRecipeByIdWorker(action) {
  try {
    yield put(recipeStartLoading());
    const recipe = yield call(recipeService.getRecipeById, action.payload.data);
    yield put(getRecipeByIdSucceeded(recipe));
    yield put();
  } catch (e) {
    yield put(getRecipeByIdError(e));
  } finally {
    yield put(recipeEndLoading());
  }
}

function* searchRecipesWorker(action) {
  try {
    yield put(recipeStartLoading());
    const recipes = yield call(recipeService.searchRecipes, action.payload.data);
    yield put(searchRecipesSucceeded(recipes));
  } catch (e) {
    yield put(searchRecipesError(e));
  } finally {
    yield put(recipeEndLoading());
  }
}

function* createRecipeWorker(action) {
  try {
    yield put(recipeStartLoading());
    console.log('Saga', action.payload.data);
    const recipe = yield call(recipeService.createRecipe, action.payload.data);
    yield put(createRecipeSuccess(recipe));
    yield call(history.push, `/recipes/${recipe._id}`);
  } catch (e) {
    yield put(createRecipeError(e));
  } finally {
    yield put(recipeEndLoading());
  }
}


export const recipeSagas = [
  takeEvery(GET_RECIPES, getRecipesWorker),
  takeEvery(GET_RECIPE_BY_ID, getRecipeByIdWorker),
  takeEvery(SEARCH_RECIPES, searchRecipesWorker),
  takeEvery(CREATE_RECIPE, createRecipeWorker),
];
