const express = require('express');
const config = require('./config');

async function startServer() {
  const app = express();
  await require('./loaders')(app);

  app.listen(config.port, (err) => {
    if (err) {
      console.log('Server can not be started', err.message);
      global.process.exit(1);
    }
    console.log(`Server has been started on port ${config.port}...`);
  });
}

startServer();
