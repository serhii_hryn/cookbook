const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
  title: { type: String, required: true },
  description: String,
  parentId: { type: Types.ObjectId, default: null },
  type: { type: String, required: true },
  createdDate: { type: Date, default: Date.now },
});

module.exports = model('Recipe', schema);
