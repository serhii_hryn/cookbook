const express = require('express');
const routes = require('../api');

const expressLoader = app => {
  app.use(express.json({ extended: true }));
  app.use('/api', routes());
};

module.exports = expressLoader;
