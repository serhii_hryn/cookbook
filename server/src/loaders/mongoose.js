const mongoose = require('mongoose');
const config = require('../config');

const mongooseLoader = async () => {
  await mongoose.connect(config.mongodbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  console.log('Successfully connected to MongoDB');
};

module.exports = mongooseLoader;
