const { Router } = require('express');
const recipe = require('./routes/recipe.routes');

module.exports = () => {
  const router = Router();
  recipe(router);

  return router;
};
