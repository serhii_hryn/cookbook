const { Router } = require('express');
const recipeService = require('../../services/recipe.service');
// const Recipe = require('../models/Recipe');

const route = Router();

module.exports = router => {
  router.use('/recipe', route);

  route.get('/', async (req, res) => {
    try {
      const recipes = await recipeService.getRecipes();
      res.status(200).json(recipes);
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during fetching recipes' });
    }
  });

  route.get('/:id', async (req, res) => {
    try {
      const recipe = await recipeService.getRecipeById(req.params.id);
      res.status(200).json(recipe);
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during fetching recipe' });
    }
  });

  route.post('/search', async (req, res) => {
    try {
      console.log('Body', req.body);
      const recipes = await recipeService.searchRecipes(req.body);
      res.status(200).json(recipes);
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during searching recipes' });
    }
  });

  route.post('/', async (req, res) => {
    try {
      const recipe = await recipeService.createRecipe(req.body);
      res.status(201).json(recipe);
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during recipe creation' });
    }
  });

  route.put('/:id?', async (req, res) => {
    // TODO add middleware for validating req.params.id
    const id = req.params.id;

    if (!id) {
      return res.status(400).json({ message: 'Recipe ID is required' });
    }

    try {
      const updatedRecipe = await recipeService.updateRecipe(id);
      res.status(200).json(updatedRecipe);
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during updating recipe' });
    }
  });

  route.delete('/:id?', async (req, res) => {
    // TODO add middleware for validating req.params.id
    const id = req.params.id;

    if (!id) {
      return res.status(400).json({ message: 'Recipe ID is required' });
    }

    try {
      await recipeService.deleteRecipe(id);
      res.status(200).json({ message: 'Recipe was deleted successfully' });
    } catch (e) {
      res.status(500).json({ message: 'Something went wrong during updating recipe' });
    }
  });
};

// const router = Router();

// router.get('/', async (req, res) => {
//   try {
//     const recipes = await Recipe.find({});
//     res.status(200).json(recipes);
//   } catch (e) {
//     req.status(500).json({ message: 'Something went wrong during fetching recipes', error: e.message });
//   }
// });

// router.post('/', async (req, res) => {
//   try {
//     const { title, description, parentId } = req.body;
//     const recipe = new Recipe({ title, description, parentId });

//     await recipe.save();

//     res.status(201).json(recipe);
//   } catch (e) {
//     res.status(500).json({ message: 'Something went wrong during recipe creation', error: e.message });
//   }
// });

// router.put('/:id', async (req, res) => {
//   try {
//     const recipeId = req.params.id;
//     if (!recipeId) {
//       res.status(400).json({ message: 'Recipe Id is required' });
//     }

//     const { title, description, parentId } = req.body;
//     const updatedRecipe = await Recipe.findByIdAndUpdate(
//       recipeId,
//       { title, description, parentId },
//       { new: true, useFindAndModify: false }
//     );

//     if (!updatedRecipe) {
//       return res.status(400).json({ message: 'Recipe with provided Id not found' });
//     }

//     res.status(200).json(updatedRecipe);
//   } catch (e) {
//     res.status(500).json({ message: 'Something went wrong during updating recipe', error: e.message });
//   }
// });

// // router.delete('/:id', async (req, res) => {
// //   try {

// //   } catch (e) {

// //   }
// // });

// module.exports = router;
