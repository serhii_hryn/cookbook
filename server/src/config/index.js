const dotenv = require('dotenv');

const result = dotenv.config();

if (result.error) throw new Error('Server: The .env file was not found');

const { port, mongodbUri, baseUrl } = result.parsed;

module.exports = {
  port,
  mongodbUri,
  baseUrl,
};
