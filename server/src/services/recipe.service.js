const Recipe = require('../models/Recipe');

module.exports = {
  getRecipes() {
    return Recipe.find({});
  },
  getRecipeById(id) {
    return Recipe.findById(id);
  },
  searchRecipes(searchBody = {}) {
    return Recipe.find(searchBody);
  },
  createRecipe(recipeDTO) {
    return new Recipe(recipeDTO).save();
  },
  updateRecipe(id, body) {
    return Recipe.findByIdAndUpdate(id, body, { new: true, useFindAndModify: false });
  },
  deleteRecipe(id) {
    return Recipe.findByIdAndDelete(id);
  },
};
